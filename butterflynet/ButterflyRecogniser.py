"""Recogniser objects """


class BaseRecogniser(object):
    def __init__(self, data: bytes):
        self.__data = data

    def recognise(self, data):
        return False


class HeaderRecogniser(BaseRecogniser):
    def __init__(self, data: bytes, length: int=None):
        super().__init__(data)
        if length is None:
            self.__len = len(data)
        else:
            self.__len = length

    def recognise(self, data: bytes):
        if len(data) < self.__len:
            return False
        else:
            if data[0:self.__len] == self.__data:
                return True
        return False


class ContainsRecogniser(BaseRecogniser):
    def recognise(self, data: bytes):
        if data in self.__data:
            return True
        return False


class AnyRecogniser(BaseRecogniser):
    def recognise(self, data: bytes):
        return True
