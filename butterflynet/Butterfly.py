# The Butterfly client file.

import socket
import multiprocessing

import butterflynet.ButterflyRecogniser


class ButterflyAbstractBase(object):
    def __init__(self):
        pass

    def connect(self, host: str, port: int):
        pass

    def disconnect(self):
        pass


class ButterflyConnector(ButterflyAbstractBase):
    """
    A basic butterfly connector.
    TODO: Expand this docstring.
    """

    def __init__(self, udp: bool=False, buffer: int=1024):
        super().__init__()
        # Create a new socket.

        # Check if we're UDP.
        if udp:
            # AF_INET -> Internet
            # SOCK_DGRAM -> UDP.
            self.__socket = socket.socket(family=socket.AF_INET, proto=socket.SOCK_DGRAM)
        else:
            # AF_INET -> Internet
            # SOCK_STREAM -> TCP
            self.__socket = socket.socket(family=socket.AF_INET, proto=socket.SOCK_STREAM)

        # Set our buffer.
        self.__buffer = buffer

        # Set the initial connection state.
        self.__connected = False

        # Create a list to store our handlers.
        self.__handlers = []

        # Create a manager.
        self.__manager = multiprocessing.Manager()

        # Create a return dict.
        self.__return_dict = self.__manager.dict()

    def connect(self, host: str, port: int):
        # Connect our socket.
        try:
            self.__socket.connect((host, port))
        except ConnectionRefusedError:
            # TODO: Add proper exception handling.
            pass
        except socket.timeout:
            # TODO: Add proper exception handling.
            pass
        else:
            self.__connected = True

    def disconnect(self):
        try:
            self.__socket.close()
        except:
            pass

    def handler(self,
                recogniser: butterflynet.ButterflyRecogniser.BaseRecogniser=butterflynet.ButterflyRecogniser.AnyRecogniser):
        def real_decorator(func):
            def inner(data):
                # Try and recognise data
                recognised = recogniser.recognise(data)
                if recognised:
                    fresult = func(data, self.__return_dict)
                else:
                    return None
                return fresult

            inner.__name__ = func.__name__
            return inner

        return real_decorator

    def start(self):
        pass

    def __listen(self):
        pass
